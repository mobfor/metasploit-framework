import json
import sys
import os
import re
import sqlite3
import hashlib

new_workflow_path = sys.argv[1]
database = sys.argv[2]

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)

    return conn

def new_wf(conn, wf_name, wf_investigator):   
    sql = ''' INSERT INTO workflows(description,investigator)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, (wf_name, wf_investigator))
    conn.commit()
    return cur.lastrowid

def new_trx(conn,wf_id,dev_id,highLvlEvt,lowLvlEvt,evidence,evidenceHash,time):
    sql = ''' INSERT INTO transactions(workflowId,deviceId,highLevelEvent,lowLevelEvent,evidence,evidenceHash,timeOfTransaction)
              VALUES(?,?,?,?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql,(wf_id,dev_id,highLvlEvt,lowLvlEvt,evidence,evidenceHash,time))
    conn.commit()

    trx_id = cur.lastrowid
    complete_trx = str(trx_id)+"|"+str(wf_id)+"|"+str(dev_id)+"|"+highLvlEvt+"|"+lowLvlEvt+"|"+evidence+"|"+str(evidenceHash)+"|"+str(time)
    hash_value = hashlib.sha256(complete_trx.encode('utf-8')).hexdigest()

    sql_hash_insert = ''' INSERT INTO hashes(transactionId,hash)
              VALUES(?,?) '''
    cur.execute(sql_hash_insert,(trx_id,hash_value))
    conn.commit()
    return trx_id

conn = create_connection(database)

for filename in os.listdir(new_workflow_path):
    if re.match(".*transactions.json", filename):
        with open(os.path.join(new_workflow_path, filename), 'r') as f:
            line_count=0
            fout=""
            for line in f:
                line_og = line
                line = line.rstrip()[1:-1]
                data = json.loads(line)

                cur_trx_id = data['transactionId']
                cur_wf_id = data['workflowId']

                if line_count==0:
                    line_count+=1
                    new_wf_id = new_wf(conn,data['workflowDescription'],data['investigator'])
                    fout = open(os.path.join(new_workflow_path,"wf_id_"+str(new_wf_id)+"_transactions.json"),"w+")
                    
                new_trx_id = new_trx(conn,new_wf_id,data['deviceId'],data['highLevelEvent'],data['lowLevelEvent'],data['evidence'],data['evidenceHash'],data['timeOfTransaction'])
                fout.write(line_og.replace('"transactionId": '+str(cur_trx_id)+', "workflowId": '+str(cur_wf_id),'"transactionId": '+str(new_trx_id)+', "workflowId": '+str(new_wf_id)))
            # fout.close()