import json
import glob
import sys
import os
from datetime import datetime
import pytz

read_files = glob.glob(sys.argv[1]+"/*.jitmflog")
output_dir = sys.argv[2]
lines_seen = set()
id_name_map = {}

def create_json(evidence_object, date_time):
    entry={}
    evidence_json = json.loads(evidence_object)
    entry["to_id"] = evidence_json["to_id"]
    entry["from_id"] = evidence_json["from_id"]
    entry["message_id"] = evidence_json["message_id"]
    entry["date"] = date_time
    entry["text"] = evidence_json["text"]
    return json.dumps(entry)

def check_entry(evidence_object, data, event_type):
    add = True
    evidence_json = json.loads(evidence_object)
  
    if len(evidence_json['to_name']) <=0: #adds name if id exists in map and entry has no name
        try:
            evidence_json["to_name"]=id_name_map[evidence_json['to_id']]
        except:
            pass

    if len(evidence_json['from_name']) <=0: #adds name if id exists in map and entry has no name
        try:
            evidence_json["from_name"]=id_name_map[evidence_json['from_id']]
        except:
            pass

    if isinstance(json.dumps(evidence_json['message_id']).strip('"'), int) and int(json.dumps(evidence_json['message_id']).strip('"')) < 0: #removes entries where message id <0
        add &= False
    if "Sent" in event_type:
        time = evidence_json["date"]
    else:
        time = data["time"]
    
    entry = create_json(evidence_object, time)
    if entry in lines_seen: #removes entry if duplicate
        add &= False

    return (add,evidence_json,entry)

def populate_map(evidence_json): #adds names to ids in map
    if len(evidence_json['to_name']) >0:
        id_name_map[evidence_json['to_id']]=evidence_json['to_name']
    if len(evidence_json['from_name']) >0:
        id_name_map[evidence_json['from_id']]=evidence_json['from_name'] 
   
def timestamp_from_datetime(dt):
    return dt.replace(tzinfo=pytz.utc).timestamp()

def output_to_timesketch():
    now = datetime.now()
    dt_string = now.strftime("%Y-%m-%d_%H:%M:%S")
    with open(os.path.join(output_dir,dt_string+"_jitmf_output.csv"), 'a') as outfile:
        outfile.write("message,timestamp,datetime,timestamp_desc\n")
        with open(os.path.join(output_dir,"merged-logs-uniq.jitmflog")) as f:
            for line in f:
                data = json.loads(line)
                if "Sent" in data["event"]:
                    time = data["object"]["date"]
                else:
                    time = data["time"]
                if len(str(time)) >11: #checking for nanoseconds
                    event_time = datetime.utcfromtimestamp(int(time)/1000.0)
                else:
                    event_time = datetime.utcfromtimestamp(int(time))
                timestamp=timestamp_from_datetime(event_time)
                iso_date = event_time.isoformat()

                line = line.replace(',',' |')
                line = line.replace('\n','')

                string_to_write=line+","+str(timestamp)+","+iso_date+","+"Event Time\n"
                outfile.write(string_to_write)

with open(os.path.join(output_dir,"merged-logs.jitmflog"), "a") as outfile:
    for f in read_files:
        with open(f, "r") as infile:
            line_2 = ""
            not_json=False
            for line in infile:
                line=line.strip()
                if len(line.strip()) >0:
                    if not_json:
                        line_2 = line_2.strip()
                        line_2 +=  line if len(line_2) == 0 else " "+line
                    else:
                        line_2 += line
                    try: 
                        data = json.loads(line_2)
                        outfile.write(line_2+"\n")
                        line_2 = ""
                        not_json=False
                    except ValueError:
                        not_json=True

with open(os.path.join(output_dir,"merged-logs.jitmflog")) as f:
    for line in f:
        if len(line.strip())>0:
            data = json.loads(line)
            populate_map(data['object'])

outfile = open(os.path.join(output_dir,"merged-logs-uniq.jitmflog"), "w")

with open(os.path.join(output_dir,"merged-logs.jitmflog")) as f:
    for line in f:
        data = json.loads(line)
        evidence_object=json.dumps(data['object'])
        add_line, evidence_json, entry = check_entry(evidence_object, data, data["event"])
        data['object']=evidence_json
        
        if add_line: # not a duplicate
            outfile.write(json.dumps(data))
            outfile.write('\n')
            
            lines_seen.add(entry)
     
outfile.close()
output_to_timesketch()
